# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2008, 2010, 2012, 2015, 2017, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2020-10-04 17:27+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: autostartmodel.cpp:376
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "\"%1\" er ikke en absolut URL."

#: autostartmodel.cpp:379
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1\" findes ikke."

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" er ikke en fil."

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\" er ikke læsbar."

#: ui/entry.qml:52
#, fuzzy, kde-format
#| msgid "Name"
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Navn"

#: ui/entry.qml:58
#, fuzzy, kde-format
#| msgid "Status"
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Status"

#: ui/entry.qml:64
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr ""

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr ""

#: ui/entry.qml:74
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "Opstart"

#: ui/entry.qml:109
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr ""

#: ui/entry.qml:113
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr ""

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr ""

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr ""

#: ui/main.qml:74
#, fuzzy, kde-format
#| msgid "Add Script..."
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "Tilføj script..."

#: ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Add Script..."
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "Tilføj script..."

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr ""

#: ui/main.qml:146
#, fuzzy, kde-format
#| msgid "&Properties..."
msgctxt "@action:button"
msgid "See properties"
msgstr "&Egenskaber..."

#: ui/main.qml:154
#, fuzzy, kde-format
#| msgid "&Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Fje&rn"

#: ui/main.qml:167
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:170
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:173
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "Præ-KDE opstart"

#: ui/main.qml:176
#, fuzzy, kde-format
#| msgid "Logout"
msgid "Logout Scripts"
msgstr "Log ud"

#: ui/main.qml:184
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:185
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""

#: ui/main.qml:200
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "Tilføj script..."

#: ui/main.qml:220
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr ""

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr ""

#: unit.cpp:28
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "Opstart"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr ""

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr ""

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Martin Schlander"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mschlander@opensuse.org"

#~ msgid "Autostart"
#~ msgstr "Autostart"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Kontrolpanelsmodul til sessions autostarthåndtering"

#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Ophavsret © 2006–2020 Autostarthåndtering-holdet"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Vedligeholder"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#, fuzzy
#~| msgid "Advanced..."
#~ msgid "Add..."
#~ msgstr "Avanceret..."

#~ msgid "Shell script path:"
#~ msgstr "Sti til skal-script:"

#~ msgid "Create as symlink"
#~ msgstr "Opret som symlink"

#~ msgid "Autostart only in Plasma"
#~ msgstr "Autostart kun i Plasma"

#~ msgid "Command"
#~ msgstr "Kommando"

#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Kør ved"

#~ msgid "Session Autostart Manager"
#~ msgstr "Sessions autostarthåndtering"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Aktiveret"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Deaktiveret"

#~ msgid "Desktop File"
#~ msgstr "Desktop-fil"

#~ msgid "Script File"
#~ msgstr "Scriptfil"

#~ msgid "Add Program..."
#~ msgstr "Tilføj program..."

#~ msgid "Before session startup"
#~ msgstr "Før opstart af session"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr "Kun filer med \".sh\"-endelser tillades til opsætning af miljøet."

#~ msgid "Shutdown"
#~ msgstr "Nedlukning"

#~ msgid "1"
#~ msgstr "1"
