# Translation of kcm_desktoptheme to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2008, 2009, 2010, 2011, 2013, 2014.
# Bjørn Kvisli <bjorn.kvisli@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcm_desktopthemedetails\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-03 02:38+0000\n"
"PO-Revision-Date: 2014-04-22 18:33+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. i18n: ectx: label, entry (name), group (Theme)
#: desktopthemesettings.kcfg:9
#, kde-format
msgid "Name of the current Plasma Style"
msgstr ""

#: kcm.cpp:111
#, kde-format
msgid "Unable to create a temporary file."
msgstr ""

#: kcm.cpp:122
#, kde-format
msgid "Unable to download the theme: %1"
msgstr ""

#: kcm.cpp:147
#, kde-format
msgid "Theme installed successfully."
msgstr "Tema vellykket installert."

#: kcm.cpp:150 kcm.cpp:156
#, kde-format
msgid "Theme installation failed."
msgstr "Temainstallasjon mislyktes."

#: kcm.cpp:262
#, kde-format
msgid "Removing theme failed: %1"
msgstr ""

#: plasma-apply-desktoptheme.cpp:31
#, kde-format
msgid ""
"This tool allows you to set the theme of the current Plasma session, without "
"accidentally setting it to one that is either not available, or which is "
"already set."
msgstr ""

#: plasma-apply-desktoptheme.cpp:35
#, kde-format
msgid ""
"The name of the theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""

#: plasma-apply-desktoptheme.cpp:36
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr ""

#: plasma-apply-desktoptheme.cpp:49
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr ""

#: plasma-apply-desktoptheme.cpp:65
#, kde-format
msgid "The current Plasma session's theme has been set to %1"
msgstr ""

#: plasma-apply-desktoptheme.cpp:67
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr ""

#: plasma-apply-desktoptheme.cpp:75
#, kde-format
msgid "You have the following Plasma themes on your system:"
msgstr ""

#: ui/main.qml:69
#, kde-format
msgid "All Themes"
msgstr ""

#: ui/main.qml:70
#, kde-format
msgid "Light Themes"
msgstr ""

#: ui/main.qml:71
#, kde-format
msgid "Dark Themes"
msgstr ""

#: ui/main.qml:72
#, kde-format
msgid "Color scheme compatible"
msgstr ""

#: ui/main.qml:97
#, kde-format
msgid "Install from File…"
msgstr ""

#: ui/main.qml:102
#, kde-format
msgid "Get New…"
msgstr ""

#: ui/main.qml:115
#, kde-format
msgid "Follows color scheme"
msgstr ""

#: ui/main.qml:133
#, kde-format
msgid "Edit Theme…"
msgstr ""

#: ui/main.qml:140
#, kde-format
msgid "Remove Theme"
msgstr "Fjern tema"

#: ui/main.qml:147
#, kde-format
msgid "Restore Theme"
msgstr ""

#: ui/main.qml:189
#, kde-format
msgid "Open Theme"
msgstr "Åpne tema"

#: ui/main.qml:191
#, kde-format
msgid "Theme Files (*.zip *.tar.gz *.tar.bz2)"
msgstr "Temafiler (*.zip *.tar.gz *.tar.bz2)"
