# Malayalam translations for plasma-desktop package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-07 00:38+0000\n"
"PO-Revision-Date: 2019-12-12 22:16+0000\n"
"Last-Translator: Vivek KJ Pazhedath <vivekkj2004@gmail.com>\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ui/DayNightView.qml:110
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""

#: ui/DayNightView.qml:113
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr ""

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr ""

#: ui/LocationsFixedView.qml:79 ui/LocationsFixedView.qml:104
#, kde-format
msgid "Zoom in"
msgstr ""

#: ui/LocationsFixedView.qml:210
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""

#: ui/LocationsFixedView.qml:223
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr ""

#: ui/LocationsFixedView.qml:250
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr ""

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr ""

#: ui/main.qml:154
#, kde-format
msgid "Switching times:"
msgstr ""

#: ui/main.qml:157
#, kde-format
msgid "Always off"
msgstr ""

#: ui/main.qml:158
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr ""

#: ui/main.qml:159
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr ""

#: ui/main.qml:160
#, kde-format
msgid "Custom times"
msgstr ""

#: ui/main.qml:161
#, kde-format
msgid "Always on night light"
msgstr ""

#: ui/main.qml:184
#, kde-format
msgid "Day light temperature:"
msgstr ""

#: ui/main.qml:227 ui/main.qml:289
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr ""

#: ui/main.qml:232 ui/main.qml:294
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr ""

#: ui/main.qml:239 ui/main.qml:301
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr ""

#: ui/main.qml:246
#, kde-format
msgid "Night light temperature:"
msgstr ""

#: ui/main.qml:311
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr ""

#: ui/main.qml:317
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr ""

#: ui/main.qml:339
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""

#: ui/main.qml:357
#, kde-format
msgid "Begin night light at:"
msgstr ""

#: ui/main.qml:370 ui/main.qml:393
#, kde-format
msgid "Input format: HH:MM"
msgstr ""

#: ui/main.qml:380
#, kde-format
msgid "Begin day light at:"
msgstr ""

#: ui/main.qml:402
#, kde-format
msgid "Transition duration:"
msgstr ""

#: ui/main.qml:411
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: ui/main.qml:424
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr ""

#: ui/main.qml:442
#, kde-format
msgid "Error: Transition time overlaps."
msgstr ""

#: ui/main.qml:465
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr ""

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr ""
#~ "shijualexonline@gmail.com,snalledam@dataone.in,vivekkj2004@gmail.com"
