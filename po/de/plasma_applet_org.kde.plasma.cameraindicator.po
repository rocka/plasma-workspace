# German translations for plasma-workspace package
# German translation for plasma-workspace.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# SPDX-FileCopyrightText: 2023 Johannes Obermayr <johannesobermayr@gmx.de>
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-26 01:47+0000\n"
"PO-Revision-Date: 2023-12-17 18:39+0100\n"
"Last-Translator: Johannes Obermayr <johannesobermayr@gmx.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: contents/ui/FullRepresentation.qml:40
#, kde-format
msgctxt "@title"
msgid "Camera"
msgstr "Kamera"

#: contents/ui/FullRepresentation.qml:43
#, kde-format
msgctxt "@info:status"
msgid "Active"
msgstr "Aktiv"

#: contents/ui/FullRepresentation.qml:45
#, kde-format
msgctxt "@info:status"
msgid "Idle"
msgstr "Leerlauf"

#: contents/ui/FullRepresentation.qml:47
#, kde-format
msgctxt "@info:status"
msgid "Error"
msgstr "Fehler"

#: contents/ui/FullRepresentation.qml:49
#, kde-format
msgctxt "@info:status"
msgid "Creating"
msgstr ""

#: contents/ui/FullRepresentation.qml:51
#, kde-format
msgctxt "@info:status"
msgid "Suspended"
msgstr "Angehalten"

#: contents/ui/FullRepresentation.qml:53
#, kde-format
msgctxt "@info:status"
msgid "Unknown"
msgstr "Unbekannt"

#: contents/ui/FullRepresentation.qml:69
#, kde-format
msgctxt "@info:status %1 camera name"
msgid "%1 is in use"
msgstr "%1 wird verwendet"

#: contents/ui/main.qml:21
#, kde-format
msgctxt "@info:tooltip"
msgid "Camera indicator is unavailable"
msgstr ""

#: contents/ui/main.qml:23
#, kde-format
msgctxt "@info:tooltip"
msgid "A camera is in use"
msgid_plural "%1 cameras are in use"
msgstr[0] "Eine Kamera wird verwendet"
msgstr[1] "%1 Kameras werden verwendet"

#: contents/ui/main.qml:25
#, kde-format
msgctxt "@info:tooltip"
msgid "No camera is in use"
msgstr "Keine Kamera wird verwendet"
