# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023 gummi <gudmundure@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-12-10 21:54+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Guðmundur Erlingsson"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "gudmundure@gmail.com"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Frídagar"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Viðburðir"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Verkefni"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Annað"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 viðburður"
msgstr[1] "%1 viðburðir"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Engir viðburðir"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:114
#, kde-format
msgid "Days"
msgstr "Dagar"

#: calendar/qml/MonthViewHeader.qml:120
#, kde-format
msgid "Months"
msgstr "Mánuðir"

#: calendar/qml/MonthViewHeader.qml:126
#, kde-format
msgid "Years"
msgstr "Ár"

#: calendar/qml/MonthViewHeader.qml:164
#, kde-format
msgid "Previous Month"
msgstr "Fyrri mánuður"

#: calendar/qml/MonthViewHeader.qml:166
#, kde-format
msgid "Previous Year"
msgstr "Fyrra ár"

#: calendar/qml/MonthViewHeader.qml:168
#, kde-format
msgid "Previous Decade"
msgstr "Fyrri áratugur"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Í dag"

#: calendar/qml/MonthViewHeader.qml:186
#, kde-format
msgid "Reset calendar to today"
msgstr "Stilla dagatal á daginn í dag"

#: calendar/qml/MonthViewHeader.qml:197
#, kde-format
msgid "Next Month"
msgstr "Næsti mánuður"

#: calendar/qml/MonthViewHeader.qml:199
#, kde-format
msgid "Next Year"
msgstr "Næsta ár"

#: calendar/qml/MonthViewHeader.qml:201
#, kde-format
msgid "Next Decade"
msgstr "Næsti áratugur"

#: calendar/qml/MonthViewHeader.qml:257
#, kde-format
msgid "Keep Open"
msgstr "Halda opnu"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Grunnstilla…"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Skjálæsing virk"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Stillir hvort skjárinn verði læstur eftir tiltekinn tíma."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Tímamörk skjáhvílu"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Stillir hversu margar mínútur líða þar til skjárinn er læstur."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Ný seta"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Síur"

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:148
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""
"Þessi græja var gerð fyrir óþekkta eldri útgáfu af Plasma og virkar ekki í "
"Plasma %1. Hafðu samband við höfund græjunnar til að fá uppfærða útgáfu."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:152
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""
"Þessi græja var gerð fyrir %1 og virkar ekki í %2. Hafðu samband við höfund "
"græjunnar til að fá uppfærða útgáfu."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:156
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""
"Þessi græja var gerð fyrir Plasma %1 og virkar ekki í Plasma %2. Uppfærðu "
"Plasma til að geta notað græjuna."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:161
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with the latest "
"version of Plasma. Please update Plasma in order to use the widget."
msgstr ""
"Þessi græja var gerð fyrir Plasma %1 og virkar ekki í nýjustu útgáfunni af "
"Plasma. Uppfærðu Plasma til að geta notað græjuna."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
msgctxt "applet category"
msgid "Accessibility"
msgstr "Aðgengi"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Forritaræsar"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Astronomy"
msgstr "Stjörnufræði"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Date and Time"
msgstr "Dagsetning og tími"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Development Tools"
msgstr "Forritunarverkfæri"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Education"
msgstr "Menntun"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Umhverfi og veður"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Examples"
msgstr "Dæmi"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "File System"
msgstr "Skráakerfi"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Leikir og fjör"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Graphics"
msgstr "Myndvinnsla"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Language"
msgstr "Tungumál"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Mapping"
msgstr "Kort"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Ýmislegt"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Multimedia"
msgstr "Margmiðlun"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Online Services"
msgstr "Netþjónustur"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Productivity"
msgstr "Framleiðni"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "System Information"
msgstr "Kerfisupplýsingar"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "Utilities"
msgstr "Nytjaforrit"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Gluggar og verkefni"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Clipboard"
msgstr "Klippispjald"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Tasks"
msgstr "Verk"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:148
#, kde-format
msgid "All Widgets"
msgstr "Allar græjur"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:152
#, kde-format
msgid "Running"
msgstr "Í keyrslu"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:158
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Hægt að fjarlægja"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:162
#, kde-format
msgid "Categories:"
msgstr "Flokkar:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:232
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Sækja nýjar Plasma-græjur"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:241
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Setja upp græju úr staðbundinni skrá..."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:503
#, kde-format
msgid "Select Plasmoid File"
msgstr "Velja græjuskrá"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Uppsetning á pakkanum %1 mistókst."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installation Failure"
msgstr "Uppsetning mistókst"
